# CamelEvent #

CamelEvent is a Web Application designed to help people organize events. It allows
the user to create and manage events, introducing the innovative idea of group
participation.

### How to run ###

In order to run the following components and template must be present in the workspace:

* Google Map
* Google Geocoding
* CropImage
* Bootstrap Style
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: camelevent
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `artevent`
--

LOCK TABLES `artevent` WRITE;
/*!40000 ALTER TABLE `artevent` DISABLE KEYS */;
/*!40000 ALTER TABLE `artevent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `camelgroup`
--

LOCK TABLES `camelgroup` WRITE;
/*!40000 ALTER TABLE `camelgroup` DISABLE KEYS */;
INSERT INTO `camelgroup` VALUES (1,NULL,'\0','ciao',1),(2,NULL,'\0','pippo',2),(3,NULL,'\0','pippo2',2),(4,'2018-02-28','','event 1',2);
/*!40000 ALTER TABLE `camelgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `camelgroup_event`
--

LOCK TABLES `camelgroup_event` WRITE;
/*!40000 ALTER TABLE `camelgroup_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `camelgroup_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `camelgroupinvitation`
--

LOCK TABLES `camelgroupinvitation` WRITE;
/*!40000 ALTER TABLE `camelgroupinvitation` DISABLE KEYS */;
INSERT INTO `camelgroupinvitation` VALUES (1,'',1,4),(2,'',3,4);
/*!40000 ALTER TABLE `camelgroupinvitation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `camelgrouppost`
--

LOCK TABLES `camelgrouppost` WRITE;
/*!40000 ALTER TABLE `camelgrouppost` DISABLE KEYS */;
/*!40000 ALTER TABLE `camelgrouppost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `camelgrouppost_camelgroup`
--

LOCK TABLES `camelgrouppost_camelgroup` WRITE;
/*!40000 ALTER TABLE `camelgrouppost_camelgroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `camelgrouppost_camelgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `cameluserinvitation`
--

LOCK TABLES `cameluserinvitation` WRITE;
/*!40000 ALTER TABLE `cameluserinvitation` DISABLE KEYS */;
INSERT INTO `cameluserinvitation` VALUES (1,'',1,2);
/*!40000 ALTER TABLE `cameluserinvitation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (1,'2018-01-31 00:00:00','event 1','2018-02-28 00:00:00',3,'\0','asd',1,2);
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `group`
--

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` VALUES (0,'admin',NULL),(1,'user',NULL);
/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `group_module`
--

LOCK TABLES `group_module` WRITE;
/*!40000 ALTER TABLE `group_module` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'event 1','41.89021,12.492230999999947','Piazza del Colosseo, 1, 00184 Roma, Italy','asdasda',41.89021,12.492230999999947);
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `location_2`
--

LOCK TABLES `location_2` WRITE;
/*!40000 ALTER TABLE `location_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `location_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `module`
--

LOCK TABLES `module` WRITE;
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
/*!40000 ALTER TABLE `module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `musicevent`
--

LOCK TABLES `musicevent` WRITE;
/*!40000 ALTER TABLE `musicevent` DISABLE KEYS */;
/*!40000 ALTER TABLE `musicevent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sponsor`
--

LOCK TABLES `sponsor` WRITE;
/*!40000 ALTER TABLE `sponsor` DISABLE KEYS */;
/*!40000 ALTER TABLE `sponsor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sponsoredevent`
--

LOCK TABLES `sponsoredevent` WRITE;
/*!40000 ALTER TABLE `sponsoredevent` DISABLE KEYS */;
/*!40000 ALTER TABLE `sponsoredevent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sportevent`
--

LOCK TABLES `sportevent` WRITE;
/*!40000 ALTER TABLE `sportevent` DISABLE KEYS */;
/*!40000 ALTER TABLE `sportevent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','admin','admin@admin.com',NULL,NULL,NULL,NULL,NULL,0),(2,'aaa','aaa','aaa@aaa.com',NULL,NULL,NULL,NULL,NULL,1),(3,'bbb','bbb','bbb@bbb.com',NULL,NULL,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user_group`
--

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-29 15:33:09

use camelevent;
-- 
-- drop database camelevent;
-- create database camelevent;

select * from user;

delete from user where oid>1;







use camelevent;


insert into `group` value(1, "user", NULL);
insert into `group` value(2, "admin", NULL);
insert into `group` value(3, "sponsor", NULL);

update user set group_oid=3 where oid>=7;
select oid, username, password, group_oid from camelevent.`user`;


insert into user (user.oid, user.username, user.password, user.email, user.group_oid) value (1, "admin", "admin", "admin@admin.com", 0);
insert into user (user.oid, user.username, user.password, user.email, user.group_oid) value (2, "aaa", "aaa", "aaa@aaa.com", 1);
insert into user (user.oid, user.username, user.password, user.email, user.group_oid) value (3, "bbb", "bbb", "bbb@bbb.com", 1);
insert into user value (3, "bbb", "bbb", "bbb@bbb.com", 1);

-- insert into group_module value (1, 1);



select * from camelevent.`group`;
select * from camelevent.`user`;
select * from camelevent.`sponsor`;
select * from camelevent.`module`;
select * from camelevent.`user_group`;
select * from camelevent.`group_module`;

insert into camelevent.`group` value (2,"admin", null);



insert into module value (4,"area4", "Sponsor Location");
insert into module value (5,"area6", "Sponsor Event");
insert into module value (6,"area7", "Sponsor Profile");

insert into group_module value (3,4);
insert into group_module value (3,5);
insert into group_module value (3,6);

insert into group_module value (0,3);

delete from camelevent.`group` where oid=0;

update group_module set group_oid=2 where group_oid=0;

select * from camelevent.`camelgroupinvitation`;
select * from camelevent.`cameluserinvitation`;
select * from camelevent.`camelgrouppost`;
select * from camelevent.`camelgroup`;
select * from camelevent.`event`;
select * from camelevent.`camelgroup_event`;
select * from camelevent.`location`;
select * from camelevent.`location_2`;
select * from camelevent.`sponsoredevent`;
select * from camelevent.`sponsoredevent_user`;

delete from camelevent.`eventimage` where event_oid is null;

alter table eventimage drop column sponsoredevent_oid;

alter table eventimage drop foreign key fk_eventimage_sponsoredevent;

delete from user where oid >5;


insert into camelevent.`sponsoredevent_user` value (2, 1);


update camelevent.`location_2` set sponsor_oid=1;
update camelevent.`event` set private=0 where oid=1;

delete from location_2 where oid>=2;


select * from camelevent.`profile_picture`;

insert into camelevent.`profile_picture` value (0, 'upload/default.jpg');
insert into camelevent.`profile_picture` value (25, 'upload/main.jpg');

update camelevent.`user` set active=true;

update camelevent.`user`set group_oid=2 where oid =1;

insert into camelevent.`cameluserinvitation` value (5,0,2,3);



delete from user where oid > 3;
delete from camelevent.`cameluserinvitation` where oid = 15;


delete from cameluserinvitation where accepted = false;

delete from cameluserinvitation where accepted = false;

select oid from camelevent.`cameluserinvitation` where accepted=true and event_oid =1 and user_oid=3;

select * 
from camelgroupinvitation, camelgroup
where camelgroupinvitation.camelgroup_oid = camelgroup.oid
;

delete from invitation where camelgroup_oid = 3;
delete from camelgroup_event where camelgroup_oid = 4;

insert into invitation value (1,1, false);
insert into invitation value (1,2, false);
insert into camelgroup value (1, null,0,"ciao",1);

select user.oid, user.username, user.email, invitation.camelgroup_oid, invitation.accepted
from user left join invitation on (user.oid = invitation.user_oid)
where user.oid != 2 and 
invitation.camelgroup_oid = 3

 and 
invitation.accepted is null and
user.username like '%ad%'
;





select user.oid, user.username, user.email 
from user 
where user.username like '%a%' and user.oid != 2 and
not exists (select * from invitation where invitation.user_oid = user.oid and invitation.camelgroup_oid = 3);

delete from invitation where camelgroup_oid = 3;





select * from camelevent.`camelgroupinvitation`;

describe camelgroupinvitation;

insert into camelgroupinvitation value (1,false, 1,2);

delete from camelgroupinvitation where user_oid = 1;








-- meeting point
select * from camelevent.`location`;

set @lat= 41.89161566498026;
set @lon = 12.49592171960444;
set @dist = 10;
set @rlon1 = @lon-@dist/abs(cos(radians(@lat))*111);
set @rlon2 = @lon+@dist/abs(cos(radians(@lat))*111);
set @rlat1 = @lat-(@dist/111);
set @rlat2 = @lat+(@dist/111);


select oid
from camelevent.`location` 
where st_within(point(lon,lat), 
envelope(linestring(point(@lon-@dist/abs(cos(radians(@lat))*111), @lat-(@dist/111)), 
point(@lon+@dist/abs(cos(radians(@lat))*111), @lat+(@dist/111)))))  
;





select astext(envelope(linestring(point(0, 0), point(1, 1))));



describe camelevent.`location` ;



select * from camelevent.`camelgroup_event`;

delete from camelgroup_event where event_oid>=4;

delete from event where oid>=4;


ALTER TABLE location DROP COLUMN mapcoord;


#input String table
#input String mapcoord
#input int dist
#output ArrayList<Integer> oids


import org.apache.commons.lang.StringEscapeUtils
import org.hibernate.*


Session session = getDBSession("db1")

String sql = "select oid from :table where st_within(point(lon,lat), envelope(linestring(point(:lon-:dist/abs(cos(radians(:lat))*111), :lat-(:dist/111)), point(:lon+:dist/abs(cos(radians(:lat))*111), :lat+(:dist/111)))))";

String[] latlon = mapcoord.split(",");

SQLQuery query = session.createSQLQuery(sql);

query.setParameter("table", table);
query.setParameter("lat", latlon[0]);
query.setParameter("lon", latlon[1]);
query.setParameter("dist", dist);

oids = ( ArrayList<Integer> ) query.list();

return oids;
